#!/usr/bin/env python
#%%
"""
Routine for preprocessing ECMWF ensemble data for minmax training.
"""

from sys import exit as q
import os,sys,re,json, time,pickle,numpy as np, pandas as pd
from netCDF4 import Dataset
from datetime import datetime, date, timedelta
import h5py as h5
import matplotlib.pyplot as plt

def get_latlon_index(lon0,lat0,lon2,lat2):
	# Returns i,j indices of (lon0,lat0) point in a (lon2,lat2) grid.
	return np.argmin(np.abs(lon2[0,:]-lon0)),np.argmin(np.abs(lat2[:,0]-lat0))

def read_nc_datetimes(ncfile,time_varname):
	ncid=Dataset(ncfile,'r')
	t=ncid.variables[time_varname]
	t0 = t.units
	mt =re.findall(r'\d{4}-\d+-\d+',t0)
	if 'seconds' in t0:
		return np.array([datetime.strptime(mt[0],'%Y-%m-%d') + timedelta(seconds=i) for i in t[:]])
	else:
		return np.array([datetime.strptime(mt[0],'%Y-%m-%d') + timedelta(hours=i) for i in t[:]])

def read_nc_var(fname,var):
	ncid=Dataset(fname,'r')
	return(np.squeeze(ncid.variables[var][:]))

def numpy_datetime64_to_UTC_datetime(x):
	return np.array([datetime.utcfromtimestamp(i.astype('O')/1e9) for i in x])

def normalize(x,mu,sigma):
	return (x-mu)/sigma

def denormalize(x,mu,sigma):
	return x*sigma+mu

def julian_date(datetimeArray):
	return np.array([date.toordinal(i) for i in datetimeArray])

#%%
# define paths:
homedir = '/home/mlicer/models/MinMax_ML/'
station=sys.argv[1]
year=sys.argv[2]
train_fname=sys.argv[3]
test_fname=sys.argv[4]

homedir = '/home/mlicer/models/MinMax_ML/'
with open("{}stationDict.json".format(homedir)) as f: # stationDict gets created by ./createStationDict.py
	stationDict = json.load(f)

stationName = stationDict[station]['name']
stationLon = stationDict[station]['lon']
stationLat = stationDict[station]['lat']

# atmospheric data:
msl_file = homedir+'atm/ecmwf_msl_adria_1h_{}.nc'.format(year)
t2m_file = homedir+'atm/ecmwf_t2m_adria_1h_{}.nc'.format(year)
u10_file = homedir+'atm/ecmwf_u10_adria_1h_{}.nc'.format(year)
v10_file = homedir+'atm/ecmwf_v10_adria_1h_{}.nc'.format(year)

eclon = read_nc_var(msl_file,'longitude')
eclat = read_nc_var(msl_file,'latitude')
eclon2,eclat2 = np.meshgrid(eclon,eclat)



dateFormat='%Y-%m-%d %H:%M:%S'

# read SSH input data:
print('Reading Air Temperature data...')
atm_dir= homedir+'atm/'
atm_file = atm_dir+'klima_podatki.txt'

cols = ['postaja','datum','idmm','ime','lat','lon','z','meanT','maxT','minT']
dateparse_obs = lambda x: pd.to_datetime(x, format=' %Y-%m-%d ', errors='coerce')
Tobs = pd.read_csv(atm_file,names=cols,sep='|',skiprows=2,parse_dates=['datum'], date_parser=dateparse_obs)
# t2m = pd.read_csv(atm_file,names=cols,sep='|',skiprows=2).dropna()
Tobs=Tobs.set_index(['datum'])
Tobs['ime']=Tobs['ime'].str.replace(" ","")
Tobs=Tobs.dropna()
Tobs['postaja']=np.squeeze([int(i) for i in Tobs['postaja']])
#%% select station: 
# station=51
Tobs0 = Tobs[Tobs['postaja']==int(station)]

# station indices in ecmwf grid:
i0,j0 = get_latlon_index(stationLon,stationLat,eclon2,eclat2)

print(stationLon, eclon2[j0,i0])
print(stationLat, eclat2[j0,i0])
#%%
#%%
print('Reading atm data...')
mslt = read_nc_datetimes(msl_file,'time')
t2mt = read_nc_datetimes(t2m_file,'time')
u10t = read_nc_datetimes(u10_file,'time')
v10t = read_nc_datetimes(v10_file,'time')

msl = read_nc_var(msl_file,'msl')
t2m = read_nc_var(t2m_file,'t2m')-273.16
u10 = read_nc_var(u10_file,'u10')
v10 = read_nc_var(v10_file,'v10')

# creating a summary JSON (i just use the this year's data - it's good enough for normalization):
norms = {
						'msl':{'mean':float(np.mean(msl)),'std':float(np.std(msl))},
						't2m':{'mean':float(np.mean(t2m)),'std':float(np.std(t2m))},
						'u10':{'mean':float(np.mean(u10)),'std':float(np.std(u10))},
						'v10':{'mean':float(np.mean(v10)),'std':float(np.std(v10))},
						}

summaryFile='summary_{}_{}.h5'.format(station,stationName)
with open(summaryFile, 'w') as fp:
	json.dump(norms, fp, indent=4, sort_keys=True)

# standardize / normalize all weather arrays:
msl = normalize(msl,norms['msl']['mean'],norms['msl']['std'])
t2m = normalize(t2m,norms['t2m']['mean'],norms['t2m']['std'])
u10 = normalize(u10,norms['u10']['mean'],norms['u10']['std'])
v10 = normalize(v10,norms['v10']['mean'],norms['v10']['std'])
Tobs0['meanT_norm'] = normalize(Tobs0.meanT,norms['t2m']['mean'],norms['t2m']['std'])
Tobs0['maxT_norm'] = normalize(Tobs0.maxT,norms['t2m']['mean'],norms['t2m']['std'])
Tobs0['minT_norm'] = normalize(Tobs0.minT,norms['t2m']['mean'],norms['t2m']['std'])

# extract normalized synced ECMWF temperature at station location:
ec_t2m0 = t2m[:,j0,i0]
# resample to daily maximums and minimums:
ecTminmax = pd.DataFrame(columns=('dates','ecT','ecTmin','ecTmax'))
ecTminmax['dates'] = mslt
ecTminmax['ecT'] = ec_t2m0
ecTminmax = ecTminmax.set_index(['dates'])
ecTminmax['ecTmax'] = ecTminmax.resample('D').max()
ecTminmax['ecTmin'] = ecTminmax.resample('D').min()
ecTminmax = ecTminmax.dropna().drop(['ecT'],axis=1)


#%% sync weather and ssh data:
print('Syncing ECMWF-OBS data...')

ectimes = pd.DataFrame()
ectimes['dates']=mslt
ectimes = ectimes.set_index(['dates'])
T2m_this_year = Tobs0.join(ectimes,how='inner')[['minT_norm','maxT_norm']]

ecTminmax_this_year = T2m_this_year.join(ecTminmax,how='inner')[['ecTmin','ecTmax']]

common_time_indices = np.array([t in T2m_this_year.index for t in ectimes.index])

msl=msl[common_time_indices]
t2m=t2m[common_time_indices]
u10=u10[common_time_indices]
v10=v10[common_time_indices]


dates=mslt[common_time_indices]


#%%
# dates_doy = np.expand_dims([int(datetime.strftime(i,'%j'))/365 for i in dates],axis=0)
# data = np.expand_dims(np.stack((msl,t2m,u10,v10),axis=-1),axis=0)
dates_doy = np.array([int(datetime.strftime(i,'%j'))/365 for i in dates])
data = np.array(np.stack((msl,t2m,u10,v10),axis=-1))
(nt,ny,nx,nfields)=data.shape[:]
Tobs0_this_year = np.array(T2m_this_year)
print(data.shape)
print(dates_doy.shape)
print(Tobs0_this_year.shape)


os.system('rm -f {}'.format(train_fname))
f_train = h5.File(train_fname, "w")

os.system('rm -f {}'.format(test_fname))
f_test = h5.File(test_fname, "w")

# validation_fname='./validation_{}_{}.h5'.format(station,stationName)
# os.system('rm -f {}'.format(validation_fname))
# f_val = h5.File(validation_fname, "a")

# split test and train datasets:
nt_test = int(0.15 * nt)
nt_train = nt-nt_test
random_test_indices = np.random.choice(nt, size=nt_test, replace=False)
idx_test = np.zeros(nt,dtype=bool)
idx_test[random_test_indices]=True
idx_train = ~idx_test

data_test = data[idx_test,:]
dates_test = julian_date(dates[idx_test])
ecTmax_test = np.squeeze(ecTminmax_this_year['ecTmax'].values)[idx_test]
ecTmin_test = np.squeeze(ecTminmax_this_year['ecTmin'].values)[idx_test]

data_train = data[idx_train,:]
dates_train = julian_date(dates[idx_train])
ecTmax_train = np.squeeze(ecTminmax_this_year['ecTmax'].values)[idx_train]
ecTmin_train = np.squeeze(ecTminmax_this_year['ecTmin'].values)[idx_train]

dates_doy_test = dates_doy[idx_test]
dates_doy_train = dates_doy[idx_train]
T_test = Tobs0_this_year[idx_test,:]
T_train = Tobs0_this_year[idx_train,:]

plt.plot(dates_doy_test,denormalize(T_test,norms['t2m']['mean'],norms['t2m']['std']),'k',label='OBS')
plt.plot(dates_doy_test,denormalize(ecTminmax_this_year[idx_test],norms['t2m']['mean'],norms['t2m']['std']),':b',label='EC')
plt.title('OBS T2m station: {} {} year: {}'.format(station,stationName,year))
plt.grid()
plt.legend()
plt.savefig('{}OBS_EC_T2m_{}_{}_{}.png'.format(homedir,station,stationName,year))




#%% write h5 files:
f_train.attrs['station'] = station
f_train.attrs['stationName'] = stationName
f_train.create_dataset('dates',data=dates_train,maxshape=(nt_train,),chunks=True)
f_train.create_dataset('weather',data=data_train,maxshape=(nt_train, ny, nx, nfields),chunks=True)
f_train.create_dataset('dates_doy',data=dates_doy_train,maxshape=(nt_train,),chunks=True)
f_train.create_dataset('lbl_minmaxT',data=T_train,maxshape=(nt_train,2),chunks=True)
f_train.create_dataset('ecTmax',data=ecTmax_train,maxshape=(nt_train,),chunks=True)
f_train.create_dataset('ecTmin',data=ecTmin_train,maxshape=(nt_train,),chunks=True)
f_train.close()

f_test.attrs['station'] = station
f_test.attrs['stationName'] = stationName
f_test.create_dataset('dates',data=dates_test,maxshape=(nt_test,),chunks=True)
f_test.create_dataset('weather',data=data_test,maxshape=(nt_test,ny, nx, nfields),chunks=True)
f_test.create_dataset('dates_doy',data=dates_doy_test,maxshape=(nt_test,),chunks=True)
f_test.create_dataset('lbl_minmaxT',data=T_test,maxshape=(nt_test,2),chunks=True)
f_test.create_dataset('ecTmax',data=ecTmax_test,maxshape=(nt_test,),chunks=True)
f_test.create_dataset('ecTmin',data=ecTmin_test,maxshape=(nt_test,),chunks=True)
f_test.close()

# %%
