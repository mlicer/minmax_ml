#!/usr/bin/env python
#%%
"""
Routine for preprocessing ECMWF ensemble data for minmax training. It constructs a set of cases. Each case consists of:
-- weather input for [-24 hours,+72 hours]
-- station input for [-24 hours,0]
-- tidal input for [-24 hours,+72 hours]
"""
from sys import exit as q
import os,sys,re,json, time,pickle,numpy as np, pandas as pd
from netCDF4 import Dataset
from datetime import datetime, timedelta
# import cartopy.crs as ccrs
# import matplotlib
# import matplotlib.pyplot as plt
import h5py as h5
import tensorflow as tf

def read_nc_datetimes(ncfile,time_varname):
	ncid=Dataset(ncfile,'r')
	t=ncid.variables[time_varname]
	t0 = t.units
	mt =re.findall(r'\d{4}-\d+-\d+',t0)
	if 'seconds' in t0:
		return np.array([datetime.strptime(mt[0],'%Y-%m-%d') + timedelta(seconds=i) for i in t[:]])
	else:
		return np.array([datetime.strptime(mt[0],'%Y-%m-%d') + timedelta(hours=i) for i in t[:]])

def read_nc_var(fname,var):
	ncid=Dataset(fname,'r')
	return(np.squeeze(ncid.variables[var][:]))

def numpy_datetime64_to_UTC_datetime(x):
	return np.array([datetime.utcfromtimestamp(i.astype('O')/1e9) for i in x])

def read_ssh(fname):
	print(fname)
	dateparse = lambda x: datetime.strptime(x, '%Y-%m-%d %H:%M:%S')
	ssh_df = pd.read_csv(fname,names=['dates','ssh','surge','surge_ssh'],sep=',',skiprows=1,parse_dates=['dates'], date_parser=dateparse)
	ssh_df['dates']=pd.to_datetime(ssh_df['dates'],format='%Y-%m-%d %H:%M:%S')
	ssh_df = ssh_df.drop(['surge','surge_ssh'],axis=1)
	ssh_df = ssh_df.dropna()
	ssh_df = ssh_df.set_index(['dates'])
	return ssh_df

def read_tide(fname):
	print(fname)
	dateparse = lambda x: datetime.strptime(x, '%Y-%m-%dT%H:%M:%S')
	ssh_df = pd.read_csv(fname,names=['dates','tide'],sep=' ',skiprows=1,parse_dates=['dates'], date_parser=dateparse)
	ssh_df['dates']=pd.to_datetime(ssh_df['dates'],format='%Y-%m-%dT%H:%M:%S')
	ssh_df = ssh_df.dropna()
	ssh_df = ssh_df.set_index(['dates'])
	return ssh_df	

def ecmwf_to_xarray(nparray,timevar):
	return xr.DataArray(
	data=nparray,
	dims=["dates", "y", "x"],
	coords=dict(
		x=(["x"],[i for i in range(nparray.shape[2])]),
		y=(["y"], [i for i in range(nparray.shape[1])]),
		dates=timevar,)
	)

def normalize(x,mu,sigma):
	return (x-mu)/sigma


#%%
# define paths:
homedir = '/home/mlicer/models/MinMax_ML/'
atm_tmin=-24
atm_tmax=72
T_tmin = -24
T_tmax = 0
prediction_horizon=72

yearmin=2007
yearmax=2010

dateFormat='%Y-%m-%d %H:%M:%S'

# read SSH input data:
print('Reading Air Temperature data...')
atm_dir= homedir+'atm/'
atm_file = atm_dir+'klima_podatki.txt'

cols = ['postaja','datum','idmm','ime','lat','lon','z','meanT','maxT','minT']
dateparse_obs = lambda x: pd.to_datetime(x, format=' %Y-%m-%d ', errors='coerce')
Tobs = pd.read_csv(atm_file,names=cols,sep='|',skiprows=2,parse_dates=['datum'], date_parser=dateparse_obs)
# t2m = pd.read_csv(atm_file,names=cols,sep='|',skiprows=2).dropna()
Tobs=Tobs.set_index(['datum'])
Tobs['ime']=Tobs['ime'].str.replace(" ","")
Tobs=Tobs.dropna()
Tobs['postaja']=np.squeeze([int(i) for i in Tobs['postaja']])
print(Tobs)
#%% select krvavec: 
station=3
Tobs0 = Tobs[Tobs['postaja']==station]
stationName=Tobs.ime[0]
#%%

# Create normalized feature for t2m: (we don't need separate mu, sigma for minT and maxT, normalization is good enough)
meanT_mean = Tobs0.meanT.mean()
meanT_std = Tobs0.meanT.std()

Tobs0['meanT_norm'] = normalize(Tobs0.meanT,meanT_mean,meanT_std)
Tobs0['maxT_norm'] = normalize(Tobs0.maxT,meanT_mean,meanT_std)
Tobs0['minT_norm'] = normalize(Tobs0.minT,meanT_mean,meanT_std)


train_fname='./train_{}_{}.h5'.format(station,stationName)
os.system('rm -f {}'.format(train_fname))
f_train = h5.File(train_fname, "a")

validation_fname='./validation_{}_{}.h5'.format(station,stationName)
os.system('rm -f {}'.format(validation_fname))
f_val = h5.File(validation_fname, "a")
#%%

for year in range(yearmin,yearmax+1):

# year=yearmin
	print(year)
	# atmospheric data:
	msl_file = homedir+'atm/ecmwf_msl_adria_1h_{}.nc'.format(year)
	t2m_file = homedir+'atm/ecmwf_t2m_adria_1h_{}.nc'.format(year)
	u10_file = homedir+'atm/ecmwf_u10_adria_1h_{}.nc'.format(year)
	v10_file = homedir+'atm/ecmwf_v10_adria_1h_{}.nc'.format(year)


	print('Reading atm data...')
	mslt = read_nc_datetimes(msl_file,'time')
	t2mt = read_nc_datetimes(t2m_file,'time')
	u10t = read_nc_datetimes(u10_file,'time')
	v10t = read_nc_datetimes(v10_file,'time')

	msl = read_nc_var(msl_file,'msl')
	t2m = read_nc_var(t2m_file,'t2m')
	u10 = read_nc_var(u10_file,'u10')
	v10 = read_nc_var(v10_file,'v10')

	#%% creating a summary JSON (i just use the first year data):
	if year == yearmin:
		summary = {
								'msl':{'mean':float(np.mean(msl)),'std':float(np.std(msl))},
								't2m':{'mean':float(np.mean(t2m)),'std':float(np.std(t2m))},
								'u10':{'mean':float(np.mean(u10)),'std':float(np.std(u10))},
								'v10':{'mean':float(np.mean(v10)),'std':float(np.std(v10))},
								'meanTobs':{'mean':float(meanT_mean),'std':float(meanT_std)},
								}
	
		with open('summary.json', 'w') as fp:
			json.dump(summary, fp, indent=4, sort_keys=True)

	# q()
	#%% standardize / normalize all weather arrays:
	msl = normalize(msl,summary['msl']['mean'],summary['msl']['std'])
	t2m = normalize(t2m,summary['t2m']['mean'],summary['t2m']['std'])
	u10 = normalize(u10,summary['u10']['mean'],summary['u10']['std'])
	v10 = normalize(v10,summary['v10']['mean'],summary['v10']['std'])

	# sync weather and ssh data:
	print('Syncing atm-ssh data...')

	ectimes = pd.DataFrame()
	ectimes['dates']=mslt
	ectimes = ectimes.set_index(['dates'])
	T2m_this_year = Tobs0.join(ectimes,how='inner')

	common_time_indices = np.array([t in T2m_this_year.index for t in ectimes.index])

	msl=msl[common_time_indices]
	t2m=t2m[common_time_indices]
	u10=u10[common_time_indices]
	v10=v10[common_time_indices]

	dates=mslt[common_time_indices]

	#%%
	print('Creating training cases...')

	weatherTimeDownsamplingStep=4
	weatherSpaceDownsamplingStep=2
	weatherDateRange = range(np.abs(atm_tmin), len(dates)-atm_tmax, 1)

	dates_cases = [[datetime.strftime(i,dateFormat).encode('ascii','ignore') for i in dates[ t : t + prediction_horizon ]] for t in weatherDateRange]


	msl_cases = [msl[ t-np.abs(atm_tmin) : t+atm_tmax : weatherTimeDownsamplingStep,
		::weatherSpaceDownsamplingStep, ::weatherSpaceDownsamplingStep] for t in weatherDateRange]
	t2m_cases = [t2m[ t-np.abs(atm_tmin) : t+atm_tmax : weatherTimeDownsamplingStep,
		::weatherSpaceDownsamplingStep, ::weatherSpaceDownsamplingStep] for t in weatherDateRange]
	u10_cases = [u10[ t-np.abs(atm_tmin) : t+atm_tmax : weatherTimeDownsamplingStep,
		::weatherSpaceDownsamplingStep, ::weatherSpaceDownsamplingStep] for t in weatherDateRange]
	v10_cases = [v10[ t-np.abs(atm_tmin) : t+atm_tmax : weatherTimeDownsamplingStep,
		::weatherSpaceDownsamplingStep, ::weatherSpaceDownsamplingStep] for t in weatherDateRange]

	#%% create datetime vectors of cases:
	print('Creating feature training cases...')
	meanT_cases = [T2m_this_year.meanT[ t-np.abs(atm_tmin) : t : 1] for t in weatherDateRange]
	maxT_cases = [T2m_this_year.maxT[ t-np.abs(atm_tmin) : t : 1] for t in weatherDateRange]
	minT_cases = [T2m_this_year.minT[ t-np.abs(atm_tmin) : t : 1] for t in weatherDateRange]

	#%% create ssh & datetime LABEL vectors of cases: SSH contains data for EVERY HOUR for times 
	# [0, +72hours] after time t (for t in WEATHER (!) date range):

	print('Creating label training cases...')
	lbl_meanT_cases = [T2m_this_year.meanT_norm[ t : t+prediction_horizon : 1] for t in weatherDateRange]
	lbl_maxT_cases = [T2m_this_year.maxT_norm[ t : t+prediction_horizon : 1] for t in weatherDateRange]
	lbl_minT_cases = [T2m_this_year.minT_norm[ t : t+prediction_horizon : 1] for t in weatherDateRange]


	# last year is validation year:
	if year==yearmax:
		dates_val = np.array(dates_cases)
		msl_val = np.array(msl_cases)
		t2m_val = np.array(t2m_cases)
		u10_val = np.array(u10_cases)
		v10_val = np.array(v10_cases)

		meanT_val =  np.expand_dims(np.array([normalize(meanT_cases[i],summary['meanTobs']['mean'],summary['meanTobs']['std']) for i in range(len(meanT_cases))]),axis=-1)
		maxT_val =  np.expand_dims(np.array([normalize(maxT_cases[i],summary['meanTobs']['mean'],summary['meanTobs']['std']) for i in range(len(maxT_cases))]),axis=-1)
		minT_val =  np.expand_dims(np.array([normalize(minT_cases[i],summary['meanTobs']['mean'],summary['meanTobs']['std']) for i in range(len(minT_cases))]),axis=-1)
		lbl_meanT_val =  np.expand_dims(np.array([normalize(lbl_meanT_cases[i],summary['meanTobs']['mean'],summary['meanTobs']['std']) for i in range(len(lbl_meanT_cases))]),axis=-1)
		lbl_maxT_val =  np.expand_dims(np.array([normalize(lbl_maxT_cases[i],summary['meanTobs']['mean'],summary['meanTobs']['std']) for i in range(len(lbl_maxT_cases))]),axis=-1)
		lbl_minT_val =  np.expand_dims(np.array([normalize(lbl_minT_cases[i],summary['meanTobs']['mean'],summary['meanTobs']['std']) for i in range(len(lbl_minT_cases))]),axis=-1)

		nt=msl_val[0].shape[0]
		ny=msl_val[0].shape[1]
		nx=msl_val[0].shape[2]
		nfields=4

		# WEATHER
		data = np.stack((msl_val,t2m_val,u10_val,v10_val),axis=-1)
		weather_val_dset = f_val.create_dataset('weather',data=data,maxshape=(None,nt, ny, nx, nfields),chunks=True)
		dates_val_dset = f_val.create_dataset('dates',data=dates_val,maxshape=(None,prediction_horizon),chunks=True)
		meanT_val_dset = f_val.create_dataset('meanT',data=meanT_val,maxshape=(None,np.abs(atm_tmin),1),chunks=True)
		maxT_val_dset = f_val.create_dataset('maxT',data=maxT_val,maxshape=(None,np.abs(atm_tmin),1),chunks=True)
		minT_val_dset = f_val.create_dataset('minT',data=minT_val,maxshape=(None,np.abs(atm_tmin),1),chunks=True)
		lbl_meanT_val_dset = f_val.create_dataset('lbl_meanT',data=lbl_meanT_val,maxshape=(None,prediction_horizon,1),chunks=True)
		lbl_maxT_val_dset = f_val.create_dataset('lbl_maxT',data=lbl_maxT_val,maxshape=(None,prediction_horizon,1),chunks=True)
		lbl_minT_val_dset = f_val.create_dataset('lbl_minT',data=lbl_minT_val,maxshape=(None,prediction_horizon,1),chunks=True)
		print('Write validation HDF5 file {}...'.format(validation_fname))

		f_val.close()
		print("\n\nAll done. Exiting.")
		q()

	meanT_train =  np.expand_dims(np.array([normalize(meanT_cases[i],summary['meanTobs']['mean'],summary['meanTobs']['std']) for i in range(len(meanT_cases))]),axis=-1)
	maxT_train =  np.expand_dims(np.array([normalize(maxT_cases[i],summary['meanTobs']['mean'],summary['meanTobs']['std']) for i in range(len(maxT_cases))]),axis=-1)
	minT_train =  np.expand_dims(np.array([normalize(minT_cases[i],summary['meanTobs']['mean'],summary['meanTobs']['std']) for i in range(len(minT_cases))]),axis=-1)
	lbl_meanT_train =  np.expand_dims(np.array([normalize(lbl_meanT_cases[i],summary['meanTobs']['mean'],summary['meanTobs']['std']) for i in range(len(lbl_meanT_cases))]),axis=-1)
	lbl_maxT_train =  np.expand_dims(np.array([normalize(lbl_maxT_cases[i],summary['meanTobs']['mean'],summary['meanTobs']['std']) for i in range(len(lbl_maxT_cases))]),axis=-1)
	lbl_minT_train =  np.expand_dims(np.array([normalize(lbl_minT_cases[i],summary['meanTobs']['mean'],summary['meanTobs']['std']) for i in range(len(lbl_minT_cases))]),axis=-1)

	# #%%
	# print('Splitting surge / no-surge cases...')

	# surge_threshold=40
	# surge_cases = [i for i in range(len(ssh_cases)) if np.any(delta_cases[i] >surge_threshold)]
	# non_surge_cases = [i for i in range(len(ssh_cases)) if not np.any(delta_cases[i]>surge_threshold)]

	# dates_surge = np.array([dates_cases[i] for i in surge_cases])
	# dates_no_surge = np.array([dates_cases[i] for i in non_surge_cases])

	# msl_surge = np.array([msl_cases[i] for i in surge_cases])
	# msl_no_surge = np.array([msl_cases[i] for i in non_surge_cases])
	# msl=None

	# t2m_surge = np.array([t2m_cases[i] for i in surge_cases])
	# t2m_no_surge = np.array([t2m_cases[i] for i in non_surge_cases])
	# t2m=None

	# u10_surge =np.array([u10_cases[i] for i in surge_cases])
	# u10_no_surge = np.array([u10_cases[i] for i in non_surge_cases])
	# u10=None

	# v10_surge = np.array([v10_cases[i] for i in surge_cases])
	# v10_no_surge = np.array([v10_cases[i] for i in non_surge_cases])
	# v10=None

	# ssh cases need to be normalized. The rest of the variables are normalized already:
	# ssh_surge =  np.expand_dims(np.array([normalize(ssh_cases[i],summary['ssh']['mean'],summary['ssh']['std']) for i in surge_cases]),axis=-1)
	# ssh_no_surge =  np.expand_dims(np.array([normalize(ssh_cases[i],summary['ssh']['mean'],summary['ssh']['std']) for i in non_surge_cases]),axis=-1)

	# tide_surge =  np.expand_dims(np.array([normalize(tide_cases[i],summary['ssh']['mean'],summary['ssh']['std']) for i in surge_cases]),axis=-1)
	# tide_no_surge =  np.expand_dims(np.array([normalize(tide_cases[i],summary['ssh']['mean'],summary['ssh']['std']) for i in non_surge_cases]),axis=-1)
		
	# delta_surge =  np.expand_dims(np.array([normalize(delta_cases[i],summary['ssh']['mean'],summary['ssh']['std']) for i in surge_cases]),axis=-1)
	# delta_no_surge =  np.expand_dims(np.array([normalize(delta_cases[i],summary['ssh']['mean'],summary['ssh']['std']) for i in non_surge_cases]),axis=-1)

	# lbl_ssh_surge =  np.expand_dims(np.array([lbl_ssh_cases[i] for i in surge_cases]),axis=-1)
	# lbl_ssh_no_surge =  np.expand_dims(np.array([lbl_ssh_cases[i] for i in non_surge_cases]),axis=-1)

	# lbl_tide_surge =  np.expand_dims(np.array([lbl_tide_cases[i] for i in surge_cases]),axis=-1)
	# lbl_tide_no_surge =  np.expand_dims(np.array([lbl_tide_cases[i] for i in non_surge_cases]),axis=-1)

	# lbl_delta_surge =  np.expand_dims(np.array([lbl_delta_cases[i] for i in surge_cases]),axis=-1)
	# lbl_delta_no_surge =  np.expand_dims(np.array([lbl_delta_cases[i] for i in non_surge_cases]),axis=-1)

	print('Write training HDF5 file {}...'.format(train_fname))

	# h5.create_dataset('X_train', data=, compression="gzip", chunks=True, maxshape=(None,)) 
	# Non-surge cases:

	numCases = meanT_train.shape[0]
	dset=None

	nt=msl_cases[0].shape[0]
	ny=msl_cases[0].shape[1]
	nx=msl_cases[0].shape[2]
	nfields=4


	# WEATHER
	data = np.stack((msl_cases,t2m_cases,u10_cases,v10_cases),axis=-1)
	# print(data.shape)
	if year==yearmin:
		weather_dset = f_train.create_dataset('weather',data=data,maxshape=(None,nt, ny, nx, nfields),chunks=True)
		print('weather shape: {}\n'.format(weather_dset.shape))

		dates_dset = f_train.create_dataset('dates',data=dates_cases,maxshape=(None,prediction_horizon),chunks=True)
		meanT_dset = f_train.create_dataset('meanT',data=meanT_train,maxshape=(None,np.abs(atm_tmin),1),chunks=True)
		maxT_dset = f_train.create_dataset('maxT',data=maxT_train,maxshape=(None,np.abs(atm_tmin),1),chunks=True)
		minT_dset = f_train.create_dataset('minT',data=minT_train,maxshape=(None,np.abs(atm_tmin),1),chunks=True)
		lbl_meanT_dset = f_train.create_dataset('lbl_meanT',data=lbl_meanT_train,maxshape=(None,prediction_horizon,1),chunks=True)
		lbl_maxT_dset = f_train.create_dataset('lbl_maxT',data=lbl_maxT_train,maxshape=(None,prediction_horizon,1),chunks=True)
		lbl_minT_dset = f_train.create_dataset('lbl_minT',data=lbl_minT_train,maxshape=(None,prediction_horizon,1),chunks=True)

	#%%		
	else:
		# resize to accomodate current year:
		weather_dset.resize(weather_dset.shape[0]+numCases, axis=0)
		print('weather shape: {}\n'.format(weather_dset.shape))

		dates_dset.resize(dates_dset.shape[0]+numCases, axis=0)
		meanT_dset.resize(meanT_dset.shape[0]+numCases, axis=0)
		maxT_dset.resize(maxT_dset.shape[0]+numCases, axis=0)
		minT_dset.resize(minT_dset.shape[0]+numCases, axis=0)
		lbl_meanT_dset.resize(lbl_meanT_dset.shape[0]+numCases, axis=0)
		lbl_maxT_dset.resize(lbl_maxT_dset.shape[0]+numCases, axis=0)
		lbl_minT_dset.resize(lbl_minT_dset.shape[0]+numCases, axis=0)

		# append current year to the end:
		print(numCases,data.shape)
		weather_dset[-numCases:] = data
		dates_dset[-numCases:] = dates_cases
		meanT_dset[-numCases:] = np.array(meanT_train)
		maxT_dset[-numCases:] = np.array(maxT_train)
		minT_dset[-numCases:] = np.array(minT_train)
		lbl_meanT_dset[-numCases:] = np.array(lbl_meanT_train)
		lbl_maxT_dset[-numCases:] = np.array(lbl_maxT_train)
		lbl_minT_dset[-numCases:] = np.array(lbl_minT_train)


f_train.close()


# %%
