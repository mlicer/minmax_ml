#!/usr/bin/env python
#%%
"""
Routine for preprocessing ECMWF ensemble data for minmax training.
"""
from sys import exit as q
import os,sys,re,json, time,pickle,numpy as np, pandas as pd
from netCDF4 import Dataset
from datetime import datetime, timedelta
import h5py as h5


def read_nc_datetimes(ncfile,time_varname):
	ncid=Dataset(ncfile,'r')
	t=ncid.variables[time_varname]
	t0 = t.units
	mt =re.findall(r'\d{4}-\d+-\d+',t0)
	if 'seconds' in t0:
		return np.array([datetime.strptime(mt[0],'%Y-%m-%d') + timedelta(seconds=i) for i in t[:]])
	else:
		return np.array([datetime.strptime(mt[0],'%Y-%m-%d') + timedelta(hours=i) for i in t[:]])

def read_nc_var(fname,var):
	ncid=Dataset(fname,'r')
	return(np.squeeze(ncid.variables[var][:]))

def numpy_datetime64_to_UTC_datetime(x):
	return np.array([datetime.utcfromtimestamp(i.astype('O')/1e9) for i in x])

def normalize(x,mu,sigma):
	return (x-mu)/sigma

#%%
# define paths:
homedir = '/home/mlicer/models/MinMax_ML/'

dateFormat='%Y-%m-%d %H:%M:%S'

# read SSH input data:
print('Reading Air Temperature data...')
atm_dir= homedir+'atm/'
atm_file = atm_dir+'klima_podatki.txt'

cols = ['postaja','datum','idmm','ime','lat','lon','z','meanT','maxT','minT']
dateparse_obs = lambda x: pd.to_datetime(x, format=' %Y-%m-%d ', errors='coerce')
Tobs = pd.read_csv(atm_file,names=cols,sep='|',skiprows=2,parse_dates=['datum'], date_parser=dateparse_obs)
# t2m = pd.read_csv(atm_file,names=cols,sep='|',skiprows=2).dropna()
Tobs=Tobs.set_index(['datum'])
Tobs['ime']=Tobs['ime'].str.replace(" ","")
Tobs=Tobs.dropna()
Tobs['postaja']=np.squeeze([int(i) for i in Tobs['postaja']])
print(Tobs)
#%% select krvavec: 
station=3
Tobs0 = Tobs[Tobs['postaja']==station]
stationName=Tobs.ime[0]
#%%

# Create normalized feature for t2m: (we don't need separate mu, sigma for minT and maxT, normalization is good enough)
meanT_mean = Tobs0.meanT.mean()
meanT_std = Tobs0.meanT.std()

Tobs0['meanT_norm'] = normalize(Tobs0.meanT,meanT_mean,meanT_std)
Tobs0['maxT_norm'] = normalize(Tobs0.maxT,meanT_mean,meanT_std)
Tobs0['minT_norm'] = normalize(Tobs0.minT,meanT_mean,meanT_std)



#%%

# atmospheric data:
msl_file = homedir+'atm/ecmwf_msl_adria_2006-2009_1h.nc'
t2m_file = homedir+'atm/ecmwf_t2m_adria_2006-2009_1h.nc'
u10_file = homedir+'atm/ecmwf_u10_adria_2006-2009_1h.nc'
v10_file = homedir+'atm/ecmwf_v10_adria_2006-2009_1h.nc'


print('Reading atm data...')
mslt = read_nc_datetimes(msl_file,'time')
t2mt = read_nc_datetimes(t2m_file,'time')
u10t = read_nc_datetimes(u10_file,'time')
v10t = read_nc_datetimes(v10_file,'time')

msl = read_nc_var(msl_file,'msl')
t2m = read_nc_var(t2m_file,'t2m')
u10 = read_nc_var(u10_file,'u10')
v10 = read_nc_var(v10_file,'v10')

#%% creating a summary JSON :
summary = {
						'msl':{'mean':float(np.mean(msl)),'std':float(np.std(msl))},
						't2m':{'mean':float(np.mean(t2m)),'std':float(np.std(t2m))},
						'u10':{'mean':float(np.mean(u10)),'std':float(np.std(u10))},
						'v10':{'mean':float(np.mean(v10)),'std':float(np.std(v10))},
						'meanTobs':{'mean':float(meanT_mean),'std':float(meanT_std)},
						}

with open('summary.json', 'w') as fp:
	json.dump(summary, fp, indent=4, sort_keys=True)

#%% standardize / normalize all weather arrays:
msl = normalize(msl,summary['msl']['mean'],summary['msl']['std'])
t2m = normalize(t2m,summary['t2m']['mean'],summary['t2m']['std'])
u10 = normalize(u10,summary['u10']['mean'],summary['u10']['std'])
v10 = normalize(v10,summary['v10']['mean'],summary['v10']['std'])

# sync weather and ssh data:
print('Syncing ECMWF-OBS data...')

ectimes = pd.DataFrame()
ectimes['dates']=mslt
ectimes = ectimes.set_index(['dates'])
T2m_obs = Tobs0.join(ectimes,how='inner')[['minT_norm','maxT_norm']]

common_time_indices = np.array([t in T2m_obs.index for t in ectimes.index])

msl=msl[common_time_indices]
t2m=t2m[common_time_indices]
u10=u10[common_time_indices]
v10=v10[common_time_indices]

dates=mslt[common_time_indices]

T2m_obs = np.array(T2m_obs)

#%% create independent validation set outside train/test interval:
val_split_idx = int(0.90*len(dates))
dates_val = dates[val_split_idx:]
msl_val = msl[val_split_idx:]
t2m_val = t2m[val_split_idx:]
u10_val = u10[val_split_idx:]
v10_val = v10[val_split_idx:]
T2m_obs_val = T2m_obs[val_split_idx:]

# the rest is for training/testing:
dates = dates[:val_split_idx]
msl = msl[:val_split_idx]
t2m = t2m[:val_split_idx]
u10 = u10[:val_split_idx]
v10 = v10[:val_split_idx]
T2m_obs = T2m_obs[:val_split_idx]

#%%
# dates_doy = np.expand_dims([int(datetime.strftime(i,'%j'))/365 for i in dates],axis=0)
# data = np.expand_dims(np.stack((msl,t2m,u10,v10),axis=-1),axis=0)
dates_doy = np.array([int(datetime.strftime(i,'%j'))/365 for i in dates])
dates_doy_val = np.array([int(datetime.strftime(i,'%j'))/365 for i in dates_val])

data = np.array(np.stack((msl,t2m,u10,v10),axis=-1))
data_val = np.array(np.stack((msl_val,t2m_val,u10_val,v10_val),axis=-1))

(nt,ny,nx,nfields)=data.shape[:]
(nt_val,ny,nx,nfields)=data_val.shape[:]

print(data.shape)
print(dates_doy.shape)
print(T2m_obs.shape)

print(data_val.shape)
print(dates_doy_val.shape)
print(T2m_obs_val.shape)

#%%
train_fname='./train_{}_{}.h5'.format(station,stationName)
os.system('rm -f {}'.format(train_fname))
f_train = h5.File(train_fname, "a")

test_fname='./test_{}_{}.h5'.format(station,stationName)
os.system('rm -f {}'.format(test_fname))
f_test = h5.File(test_fname, "a")

validation_fname='./validation_{}_{}.h5'.format(station,stationName)
os.system('rm -f {}'.format(validation_fname))
f_val = h5.File(validation_fname, "a")

# split test and train datasets:
nt_test = int(0.25 * nt)
nt_train = nt-nt_test
random_test_indices = np.random.choice(nt, size=nt_test, replace=False)
idx_test = np.zeros(nt,dtype=bool)
idx_test[random_test_indices]=True
idx_train = ~idx_test

data_test = data[idx_test,:]
data_train = data[idx_train,:]

dates_doy_test = dates_doy[idx_test]
dates_doy_train = dates_doy[idx_train]

T_test = T2m_obs[idx_test,:]
T_train = T2m_obs[idx_train,:]

# write h5 files:
f_train.attrs['station'] = station
f_train.attrs['stationName'] = stationName
f_train.create_dataset('weather',data=data_train,maxshape=(nt_train, ny, nx, nfields),chunks=True)
f_train.create_dataset('dates_doy',data=dates_doy_train,maxshape=(nt_train,),chunks=True)
f_train.create_dataset('lbl_minmaxT',data=T_train,maxshape=(nt_train,2),chunks=True)# %%
f_train.close()

f_test.attrs['station'] = station
f_test.attrs['stationName'] = stationName
f_test.create_dataset('weather',data=data_test,maxshape=(nt_test,ny, nx, nfields),chunks=True)
f_test.create_dataset('dates_doy',data=dates_doy_test,maxshape=(nt_test,),chunks=True)
f_test.create_dataset('lbl_minmaxT',data=T_test,maxshape=(nt_test,2),chunks=True)# %%
f_test.close()

f_val.attrs['station'] = station
f_val.attrs['stationName'] = stationName
f_val.create_dataset('weather',data=data_val,maxshape=(nt_val,ny, nx, nfields),chunks=True)
f_val.create_dataset('dates_doy',data=dates_doy_val,maxshape=(nt_val,),chunks=True)
f_val.create_dataset('lbl_minmaxT',data=T2m_obs_val,maxshape=(nt_val,2),chunks=True)# %%
f_val.close()

# %%
