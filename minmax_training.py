#!/usr/bin/env python
# # %% CREATE A DEEP NETWORK with multiple categories of inputs and outputs:
#%%
import h5py, json
import tensorflow as tf
from tensorflow.keras import Input, Model
from tensorflow.keras.layers import Dense, Flatten,concatenate
from tensorflow.keras.layers import BatchNormalization, Conv2D, MaxPooling2D
from tensorflow.keras.layers import Activation
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import EarlyStopping
from datetime import datetime, date, timedelta
import sys
from sys import exit as q
import matplotlib.pyplot as plt
from tensorflow.python.platform.tf_logging import error

def denormalize(var,mu,sigma):
	return sigma*var+mu


#%%
station=sys.argv[1] #  tfmodel_3_KRVAVEC_2007.h5 
year=sys.argv[2] #  tfmodel_3_KRVAVEC_2007.h5 
model_fname=sys.argv[3] #  tfmodel_3_KRVAVEC_2007.h5 
train_fname=sys.argv[4] #  train_3_KRVAVEC_2007.h5 
test_fname=sys.argv[5] #  test_3_KRVAVEC_2007.h5 

with h5py.File(train_fname,'r') as f:
	data_train=f['weather'][:]
	dates_train = f['dates'][:]
	# dates_train = [date.fromordinal(i) for i in dates_train]
	dates_doy_train = f['dates_doy'][:]
	T_train = f['lbl_minmaxT'][:]



#%%
# define two sets of inputs
inputWeather = Input(shape=data_train.shape[1:])
inputWeather.shape
#%%
inputDates = Input(shape=(1))
inputDates.shape
#%%
# the first branch operates on the first input
conv_layer1 = Conv2D(filters=25, kernel_size=(6, 6))
conv_layer2 = Conv2D(filters=17, kernel_size=(3, 3))
conv_layer3 = Conv2D(filters=11, kernel_size=(1, 1))
maxpool_layer = MaxPooling2D(pool_size=(2, 2), strides=None)

ANNtype='MLP' # MLP = MultiLayerPerceptron , CNN  = Conv Neural Network

if ANNtype in ['CNN']:
	x = conv_layer1(inputWeather)
	# x = BatchNormalization()(x)
	x = Activation('relu')(x)
	x = maxpool_layer(x)

	x = conv_layer2(inputWeather)
	# x = BatchNormalization()(x)
	x = Activation('relu')(x)
	x = maxpool_layer(x)

	x = conv_layer3(x)
	# x = BatchNormalization()(x)
	x = Activation('relu')(x)
	x = maxpool_layer(x)

	# x = BatchNormalization()(x)

elif ANNtype in ['MLP']:
	x = Dense(80, activation="relu")(inputWeather)
	# x = BatchNormalization()(x)
	x = Dense(120, activation="relu")(x)
	# x = BatchNormalization()(x)
	x = Dense(120, activation="relu")(x)
	# x = BatchNormalization()(x)
	x = Dense(80, activation="relu")(x)
	# x = BatchNormalization()(x)
	x = Dense(4, activation="relu")(x)
	x = Flatten()(x)
	x = Model(inputs=inputWeather, outputs=x)
else:
	print('INVALID ANNtype. Ciao!')
	q()

print('x',x.output_shape)
#%%
# the second branch opreates on the second input
y = Dense(8, activation="relu")(inputDates)
y = Dense(4, activation="relu")(y)

y = Model(inputs=inputDates, outputs=y)
print('y',y.output_shape)

# combine the output of the two branches
combined = concatenate([x.output, y.output])

#%% apply a FC layer and then a regression prediction on the
# combined outputs
z = Dense(2, activation="relu")(combined)
z = Dense(2, activation="linear")(z)
# our model will accept the inputs of the two branches and
# then output two values (minT, maxT)
model = Model(inputs=[x.input, y.input], outputs=z)

# %%
# compile the model 
numEpochs=200
opt = Adam(learning_rate=3e-4, decay=1e-4 / numEpochs)
model.compile(loss="mean_squared_error", optimizer=opt)
# train the model
print("[INFO] training data...")
print(data_train.shape)
print(dates_doy_train.shape)
print(T_train.shape)
# print("[INFO] testing data...")
# print(data_test.shape)
# print(dates_doy_test.shape)
# print(T_test.shape)

#%%
es = EarlyStopping(monitor='val_loss', patience=20,restore_best_weights=True)

# history = model.fit(
# 	x=[data_train, dates_doy_train], y=T_train,
# 	validation_data=([data_test, dates_doy_test], T_test),
# 	epochs=numEpochs, batch_size=1,callbacks=[es])

history = model.fit(
	x=[data_train, dates_doy_train], y=T_train,
	validation_split=0.15,
	epochs=numEpochs, batch_size=1,callbacks=[es])

# history = model.fit(
# 	x=[data_train, dates_doy_train], y=T_train,
# 	epochs=numEpochs, batch_size=1, callbacks=[es])	

model.save(model_fname)

print(history.history)
#%% make predictions on the testing data
# print("[INFO] predicting house prices...")
# preds = model.predict([testAttrX, testImagesX])