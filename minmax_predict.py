#!/usr/bin/env python
#%%
import h5py, json, sys
import tensorflow as tf
# from tensorflow.keras import Input, Model
# from tensorflow.keras.layers import Dense, Flatten,concatenate
# from tensorflow.keras.layers import BatchNormalization
# from tensorflow.keras.optimizers import Adam
from tensorflow.keras.models import load_model
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error

def denormalize(var,mu,sigma):
	return sigma*var+mu


#%%
station =  sys.argv[1]
year = sys.argv[2]
model_fname = sys.argv[3]# tfmodel_train_3_KRVAVEC.h5
test_fname=sys.argv[4] # test_3_KRVAVEC.h5


homedir = '/home/mlicer/models/MinMax_ML/'
with open("{}stationDict.json".format(homedir)) as f: # stationDict gets created by ./createStationDict.py
	stationDict = json.load(f)

stationName = stationDict[station]['name']
stationLon = stationDict[station]['lon']
stationLat = stationDict[station]['lat']

with h5py.File(test_fname,'r') as f:
	data_test=f['weather'][:]
	dates_doy_test = f['dates_doy'][:]
	T_test = f['lbl_minmaxT'][:]
	ecTmin = f['ecTmin'][:]
	ecTmax = f['ecTmax'][:]
	station = f.attrs['station']
	stationName = f.attrs['stationName']

print(ecTmin)
print(ecTmax)

#%%
model = load_model(model_fname)


# %%
minmax_pred = model.predict([data_test,dates_doy_test])
# %% denormalize:
norms = json.load(open('./summary.json'))

dates_doy_test = dates_doy_test*365

pred_minT = denormalize(minmax_pred[:,0],norms['t2m']['mean'],norms['t2m']['std'])
pred_maxT = denormalize(minmax_pred[:,1],norms['t2m']['mean'],norms['t2m']['std'])
ecTmin = denormalize(ecTmin,norms['t2m']['mean'],norms['t2m']['std'])
ecTmax = denormalize(ecTmax,norms['t2m']['mean'],norms['t2m']['std'])
minT = denormalize(T_test[:,0],norms['t2m']['mean'],norms['t2m']['std'])
maxT = denormalize(T_test[:,1],norms['t2m']['mean'],norms['t2m']['std'])

ECmin_RMSE = mean_squared_error(minT,ecTmin,squared=False)
ECmax_RMSE = mean_squared_error(maxT,ecTmax,squared=False)
MLmin_RMSE = mean_squared_error(minT,pred_minT,squared=False)
MLmax_RMSE = mean_squared_error(maxT,pred_maxT,squared=False)
# %%
lwidth1=4
lwidth2=2
plt.rcParams['font.size'] = 20
plt.figure(figsize=(15,15))
plt.subplot(2,1,1)
plt.title('MinMax T2m at station {}'.format(stationName))
# plt.plot(dates_doy_test,pred_minT,'--b',label='ML min T')
# plt.plot(dates_doy_test,minT,'b',label='OBS min T')
plt.plot(dates_doy_test,maxT,color='darkred',linewidth=lwidth1,label='OBS max T')
plt.plot(dates_doy_test,pred_maxT,color='red',linestyle='-',linewidth=lwidth2,marker='.',label='ML max T, RMSE = {:5.1f} $^o$C'.format(MLmax_RMSE))
plt.plot(dates_doy_test,ecTmax,color='salmon',label='EC max T, RMSE = {:5.1f} $^o$C'.format(ECmax_RMSE))
plt.xlabel('Day of year')
plt.ylabel('Temperature 2m [$^o$C]')
plt.legend()
plt.grid()

plt.subplot(2,1,2)
plt.plot(dates_doy_test,minT,color='midnightblue',linewidth=lwidth1,label='OBS min T')
plt.plot(dates_doy_test,pred_minT,color='steelblue',linestyle='-',linewidth=lwidth2,marker='.',label='ML min T, RMSE = {:5.1f} $^o$C'.format(MLmin_RMSE))
plt.plot(dates_doy_test,ecTmax,color='lightsteelblue',label='EC min T, RMSE = {:5.1f} $^o$C'.format(ECmin_RMSE))
# plt.plot(dates_doy_test,pred_maxT,'--r',label='ML max T')
# plt.plot(dates_doy_test,maxT,'r',label='OBS max T')
plt.xlabel('Day of year')
plt.ylabel('Temperature 2m [$^o$C]')
plt.legend()
plt.grid()

figname='ML_minmax_pred_{}_{}.pdf'.format(stationName,year)
plt.savefig(figname,dpi=300)
print('{} written.'.format(figname))
# plt.show()
# %%
