#!/usr/bin/env python
#%%
import os, sys, pandas as pd, numpy as np, json

homedir = '/home/mlicer/models/MinMax_ML/'
def removeSlavicChars(string):
	return string.replace('Č','C').replace('Ž','Z').replace('Š','S')

stationDictFile='{}stationDict.json'.format(homedir)

if os.path.exists(stationDictFile):
	with open(stationDictFile) as f: # stationDict gets created by ./createStationDict.py
		stationDict = json.load(f)
		for station in stationDict.keys():
			print(station,stationDict[station]['name'])	
else:
	print('Reading Air Temperature data...')
	atm_dir= homedir+'atm/'
	atm_file = atm_dir+'klima_podatki.txt'

	cols = ['postaja','datum','idmm','ime','lat','lon','z','meanT','maxT','minT']
	dateparse_obs = lambda x: pd.to_datetime(x, format=' %Y-%m-%d ', errors='coerce')
	Tobs = pd.read_csv(atm_file,names=cols,sep='|',skiprows=2,parse_dates=['datum'], date_parser=dateparse_obs)
	# t2m = pd.read_csv(atm_file,names=cols,sep='|',skiprows=2).dropna()
	Tobs=Tobs.set_index(['datum'])
	Tobs['ime']=Tobs['ime'].str.replace(" ","")
	Tobs=Tobs.dropna()
	Tobs['postaja']=np.squeeze([int(i) for i in Tobs['postaja']])

	allStations = np.unique(Tobs['postaja'].values)
	#%% create station metadata dict:
	stationDict = {}
	for station in allStations:
		station=int(station)
		stationDict[station]={}
		Tobs0 = Tobs[Tobs['postaja']==station]
		stationDict[station]['name']=removeSlavicChars(Tobs0.ime[0])
		stationDict[station]['lon']=Tobs0.lon[0]
		stationDict[station]['lat']=Tobs0.lat[0]

	with open(stationDictFile,"w") as f:
		json.dump(stationDict,f)

# %%
